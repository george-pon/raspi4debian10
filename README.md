# raspi4debian10

docker image for my personal work bench.

target platform : linux/amd64 , linux/arm/v7 (raspberry pi 4)

### how to use

```
    docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        -e DOCKER_HOST=${$DOCKER_HOST} \
        registry.gitlab.com/george-pon/raspi4debian10:latest
```

