#!/bin/bash
#
# test run image
#
function docker-run-raspi4debian10() {
    ${WINPTY_CMD} docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        registry.gitlab.com/george-pon/raspi4debian10:latest
}
docker-run-raspi4debian10
