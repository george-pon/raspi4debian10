#!/bin/bash
#
# test run image
#
function docker-run-raspi4debian10() {
    docker pull registry.gitlab.com/george-pon/raspi4debian10:latest
    ${WINPTY_CMD} docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        -e DOCKER_HOST=${DOCKER_HOST} \
        registry.gitlab.com/george-pon/raspi4debian10:latest
}
docker-run-raspi4debian10
