FROM debian:buster

ENV DEBIANTOMCAT_VERSION build-target
ENV RASPI4DEBIAN10_VERSION latest
ENV RASPI4DEBIAN10_VERSION stable
ENV RASPI4DEBIAN10_VERSION raspi4-debian10
ENV RASPI4DEBIAN10_IMAGE registry.gitlab.com/george-pon/raspi4debian10

ENV DEBIAN_FRONTEND noninteractive

# set locale
RUN apt update && apt upgrade -y && \
    apt install -y locales  apt-transport-https  ca-certificates  software-properties-common && \
    localedef -i ja_JP -c -f UTF-8 -A /usr/share/locale/locale.alias ja_JP.UTF-8 && \
    apt clean all
ENV LANG ja_JP.utf8

# set timezone
RUN ln -fs /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
ENV TZ Asia/Tokyo

# install man pages
RUN apt-get install -y man-db  manpages && apt-get clean

# install etc utils
RUN apt install -y \
        bash-completion \
        connect-proxy \
        curl \
        dnsutils \
        emacs-nox \
        expect \
        gettext \
        git \
        gnupg2 \
        iproute2 \
        jq \
        less \
        lsof \
        make \
        netcat \
        net-tools \
        procps \
        python3-pip \
        python-pip \
        qemu-user-static \
        rsync \
        sudo \
        tcpdump \
        traceroute \
        tree \
        unzip \
        vim \
        w3m \
        wget \
        zip \
    && apt clean all

# install docker client
#RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
#   add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
#   apt-get update && \
#   apt-get install -y docker-ce-cli && apt-get clean


# install docker client from static binary
RUN apt-get update && \
    apt-get install -y apt-transport-https ca-certificates curl gnupg && \
    export DOCKER_VERSION=19.03.5 && \
    export MACHINE=$( uname -m ) && \
    if [ x"$MACHINE"x = x"armv7l"x ]; then MACHINE=armhf ; fi && \
    curl -LO https://download.docker.com/linux/static/stable/${MACHINE}/docker-${DOCKER_VERSION}.tgz && \
    tar xvzf docker-${DOCKER_VERSION}.tgz && \
    cp docker/docker /usr/bin/docker && \
    rm -rf docker && \
    apt-get clean

# install docker-compose
RUN apt install -y docker-compose && apt-get clean

ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENV HOME /root

RUN mkdir -p     $HOME
ADD bashrc       $HOME/.bashrc
ADD bash_profile $HOME/.bash_profile
ADD vimrc        $HOME/.vimrc
ADD emacsrc      $HOME/.emacs
ENV ENV $HOME/.bashrc

ADD bin /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh

# add sudo user
# https://qiita.com/iganari/items/1d590e358a029a1776d6 Dockerコンテナ内にsudoユーザを追加する - Qiita
# ユーザー名 debian
# パスワード hogehoge
RUN groupadd -g 1000 debian && \
    useradd  -g      debian -G sudo -m -s /bin/bash debian && \
    echo 'debian:hogehoge' | chpasswd && \
    echo 'Defaults visiblepw'            >> /etc/sudoers && \
    echo 'debian ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# use normal user debian
# USER debian

CMD ["/usr/local/bin/docker-entrypoint.sh"]

